extern crate meval;

use std::ffi::CStr;
use std::os::raw::c_char;

// This is here for testing. Never change.
pub fn mahprint() {
    println!("Just a testfunction");
}

// Most simple caller of meval, from meval readme.
pub fn eval12() {
    let r = meval::eval_str("1 + 2").unwrap();
    println!("1 + 2 = {}", r);
}

// Lets add a function callable from Julia
#[no_mangle]
pub extern fn hello() {
    println!("You called the `hello()` function!");
}

// This is what we want to do
#[no_mangle]
pub extern fn wat(exprin: *const c_char) -> f64 {
    let cstr = unsafe { CStr::from_ptr(exprin) };
    match cstr.to_str() {
        Ok(s) => {
            println!("You wanted to print: {}", s);
            // Example from meval readme
            let expr: meval::Expr = s.parse().unwrap();
            let func = expr.bind("x").unwrap();

            let vs: Vec<_> = (0..100+1).map(|i| func(i as f64 / 100.)).collect();
            let svs: f64 = vs.iter().sum();
            println!("{:?}, 0 <= x <= 1: {:?}", s, vs);
            println!("and this is the sum {:?}", svs);
            return svs;
        }
        Err(_) => {
            println!("error")
        }
    }
    return 0.0;
}

// This is what we want to do
#[no_mangle]
pub extern fn evalexpr(exprin: *const c_char) -> f64 {
    let cstr = unsafe { CStr::from_ptr(exprin) };
    match cstr.to_str() {
        Ok(s) => {
            let expr: meval::Expr = s.parse().unwrap();
            let func = expr.bind("x").unwrap();
            let vs: Vec<_> = (1..100+1).map(|i| func(i as f64 / 100.)).collect();
            let svs: f64 = vs.iter().sum();
            return svs;
        }
        Err(_) => {
            println!("error")
        }
    }
    return 0.0f64/0.0f64; // NAN for rust
}

/*
From reading the docs
https://docs.rs/meval/0.1.0/meval/
it seems, since bind_* only supports up to three variables, that I have to:
First define a context that adds all variables. The context, however, these are not variables, but they are more like constants, that is when defining them in the context one must set their values.

I think I could...
have a rust function that takes a list of symbols/as strings, and their corresponding values. Loop over them all to define the context, and from there eval_with_context()
That rust function has to be called N^n times for the N-sliced interval and n-variable expression. So, essentially need a direct-product iteration over that.
This should work.
*/

// Try and implement fitness...
#[no_mangle]
pub extern fn evalr(exprin: &str, vars: &std::vec::Vec<&str>, pnt: &std::vec::Vec<f64>) -> f64 {
    let expr: meval::Expr = exprin.parse().unwrap();
    let mut ctx = meval::Context::new();
    // Populate the context
    for i in 0..vars.len()  {
        ctx.var(vars[i], pnt[i]);
    }
    let ret = expr.eval_with_context(ctx).unwrap();
    return ret;
}
// So this ^^^^ we can call from rust. It calculates at a certain point, so Julia takes care of what points to compute it in... for now.
// But we need one callable from Julia

// #[allow(unused_variables)]
#[no_mangle]
pub extern fn evalj(exprin: *const c_char, vars: &std::vec::Vec<*const c_char>, pnt: &std::vec::Vec<f64>, len: u32) -> f64 {

    // Create the expression
    let cexpr = unsafe { CStr::from_ptr(exprin) };
    let sexpr = cexpr.to_str().unwrap();
    let expr: meval::Expr = sexpr.parse().unwrap();

    // Create variables/Populate context
    let mut ctx = meval::Context::new();
    // vars.len() is not the length as in Julia...
    for i in 0 .. len as usize {
        let cvar = unsafe { CStr::from_ptr(vars[i]) };
        let svar = cvar.to_str().unwrap();
        ctx.var(svar, pnt[i]);
    }

    // Evaluate error in point
    let ret = expr.eval_with_context(ctx).unwrap();
    // Return
    return ret;
}
