extern crate meval_caller;
// use std::ffi::{CString};

fn main() {
    meval_caller::mahprint();

    meval_caller::eval12();

    meval_caller::hello();

    let vars = &vec!["x", "y"];
    // let pnt = vec![0.5,1.];

    let mut pnts = Vec::new();
    for i in 0..10 {
        for j in 0..10 {
            pnts.push(vec![i as f64 / 10., j as f64 /10.]);
        }
    }

    let expr = "y+x+1";

    let vs: Vec<_> = pnts.iter().map(|x| meval_caller::evalr(expr,vars,x) ).collect();

    let mut s = 0f64;

    for i in vs { s+= i }

    println!("{:?}", s);

    /*
    // While this does not produce errors, it also does not give evalexpr the string correctly
    let ptr = CString::new("1hello").unwrap().as_ptr();
    meval_caller::evalexpr(ptr);
    */
}
