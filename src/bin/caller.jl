#!/usr/bin/julia

libpath = *(@__DIR__,"/../../target/debug/")
push!(Libdl.DL_LOAD_PATH,libpath)

#=
x = ccall((:hello,"libmeval_caller"),Int32,())

expression = "x*sin(x)/x"

y = ccall((:wat,"libmeval_caller"),Float64,(Cstring,), expression)

println("And there the sum is again: $(y)")
=#

##############################
#### Stuff from MetaJulia ####
##############################
ln(x) = log(x)
head = [:sin, :cos, :ln, :exp, :+, :-, :*, :/]
tail = Any[i for i=0:9]
append!(tail, [:x])

head = vcat(head, tail)

head_l = 10
tail_l = head_l+1

function callable(expr)
    try
        eval(Expr(:call, expr, 1))
        return true
    catch
        return false
    end
end

function eargs(expr)
    if expr in [:sin, :cos, :ln, :exp]
        return 1
    elseif expr in [:-, :/]
        return 2
    elseif expr in [:+, :*]
        return Inf
    else
        return NaN
    end
end

function genelist()
    elist = Any[]
    for i in 1:head_l
        push!(elist, rand(head))
    end

    for i in 1:tail_l
        push!(elist, rand(tail))
    end

    return elist
end

"""
    buildexpr(elist)
A recursive function forming mathematical expressions from an elist.
# Returns
It will return a tuple of `(Expr, nelist)`, where `Expr` is the expression that it built,
and `nelist` is the list of the unused trailing entries in the original `elist`.
"""
function buildexpr(elist)
    if eargs(elist[1]) == 1
        expr = buildexpr(elist[2:end])
        # Only one argument is taken here, the list of unused should be returned
        return Expr(:call, elist[1], expr[1]), expr[2]
    elseif eargs(elist[1]) == 2 || eargs(elist[1]) == Inf
        if isequal(eargs(elist[2]), NaN)
            # If elist[2] is a terminator (eargs() == NaN), then the expression must either
            # truncate "completely" (if also elist[3] is a terminator) or grow to the right
            # (or in the second argument, that is).
            if isequal(eargs(elist[3]), NaN)
                # Truncate completely
                return Expr(:call, elist[1], elist[2], elist[3]), elist[4:end]
            else
                expr = buildexpr(elist[3:end])
                return Expr(:call, elist[1], elist[2], expr[1]), expr[2]
            end
        else
            # If it is not a terminator at elist[2], then the expression grows to the
            # left (first argument), and the second argument is...
            expr = buildexpr(elist[2:end])
            if isequal(eargs(expr[2][1]), NaN)
                return Expr(:call, elist[1], expr[1], expr[2][1]), expr[2][2:end]
            else
                expr2 = buildexpr(expr[2])
                return Expr(:call, elist[1], expr[1], expr2[1]), expr2[2]
            end
        end

    else
        return elist[1], elist[2:end]
    end
end

function lazy_eval(expr)
    try
        return eval(expr[1])
    catch
        return Inf
    end
end

function safe_string(instring::String)
    # regex from http://www.regular-expressions.info/floatingpoint.html
    regex = r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?x"
    while typeof(match(regex, instring)) != Void
        m = match(regex, instring).match[1:end-1]
        instring = replace(instring, regex, m * " * x", 1)
    end
    return instring
end

##############################
#### Calling the rust-lib ####
##############################

expr = "(1 + exp(x) - (2 * x - ((0)+((((x)+(exp((x))))-(6))))))^2"
y = ccall((:evalexpr,"libmeval_caller"),Float64,(Cstring,), expr)
println("$(expr) evaluated to: $(y)")

vars = [Base.unsafe_convert(Cstring, Base.cconvert(Cstring, "x")), Base.unsafe_convert(Cstring, Base.cconvert(Cstring, "y"))]
pt = [0.1,0.2]
len = 2
z = ccall((:evalj,"libmeval_caller"),Float64,(Cstring,Array{Cstring,1},Array{Float64,1},UInt32), expr,vars,pt,len)
println("Just testing: $(z)")

#elist = genelist()
#expr = buildexpr(elist)[1]
#iter = 0
#while iter < 10000
#    elist = genelist()
#    expr = buildexpr(elist)[1]
#    expr = safe_string("$(expr)")
#    y = ccall((:evalexpr,"libmeval_caller"),Float64,(Cstring,), expr)
#    println("$(expr) evaluated to: $(y)")
#    iter += 1
#end
