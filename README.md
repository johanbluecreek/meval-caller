# meval-caller

## Overview

This is an example for how to build a C-compatible library with Rust and how to call it from Julia. This was just for myself to learn, and to fulfil a very particular goal. But, in the future I may make this code a bit more pedagogical.

For now, it is a Rust library that you run with
```
$ cargo run
```
which builds everything and runs the binary created from `caller.rs` that calls some of the functions of the library. After that is done, you can call
```
$ ./src/bin/caller.jl
```
which is a Julia program that generates a random (to some degree) mathematical expression (this is done by code that is copied from the [MetaJulia](https://github.com/johanbluecreek/MetaJulia) repository), and gives it over to one of the functions of the Rust library that calculates the expressions in a bunch of points using [meval-rs](https://github.com/rekka/meval-rs).

The point of publishing this code here is to provide the source code for the binary I copied into the [Genetic_DE_solver](https://github.com/johanbluecreek/Genetic_DE_solver) project (only as a proof-of-concept).
